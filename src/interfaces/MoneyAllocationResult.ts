import Money from '../Money';

interface MoneyAllocationResult {
    remainder: Money;
    allocatedFunds: Money[];
}

export default MoneyAllocationResult;
